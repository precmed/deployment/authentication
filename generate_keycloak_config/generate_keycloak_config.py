import os
roles_admin_password = os.environ['ROLES_ADMIN_PASSWORD']
roles_admin_username = os.environ['ROLES_ADMIN_USERNAME']
config_dir = os.environ['CONFIG_DIR']

import random
import base64
from backports.pbkdf2 import pbkdf2_hmac

hash_iterations = 27500
salt_size = 16
hashed_salted_size = 64
salt = bytes(random.getrandbits(8) for _ in range(salt_size))
hashed_salted = pbkdf2_hmac("sha256", roles_admin_password.encode('utf-8'), salt, 27500, hashed_salted_size)

import json

config = {
    "id": "precmed",
    "realm": "precmed",
    "enabled" : True,
    "accessTokenLifespan": 86400,
    "ssoSessionIdleTimeout": 2592000,
    "ssoSessionMaxLifespan": 2592000,
    "offlineSessionIdleTimeout": 2592000,
    "roles": {},
    "clients": [],
    "users": [{
    "username" : roles_admin_username,
    "enabled": True,
    "credentials" : [ {
        "type" : "password",
        "hashedSaltedValue" : base64.b64encode(hashed_salted).decode('utf-8'),
        "salt" : base64.b64encode(salt).decode('utf-8'),
        "hashIterations" : hash_iterations,
        "counter" : 0,
        "algorithm" : "pbkdf2-sha256",
        "digits" : 0,
        "period" : 0,
        "createdDate" : 1576833701648,
        "config" : { }
    } ],
    "clientRoles" : {
        "realm-management" : [ "view-users", "manage-clients" ]
    }
    }]
}

if ( os.environ.get('MINIO_CLIENT_SECRET') is not None) and ( os.environ.get('MINIO_REDIRECT_URI') is not None):
    minio_client_secret = os.environ['MINIO_CLIENT_SECRET']
    minio_redirect_uri = os.environ['MINIO_REDIRECT_URI']
    new_client = {
        "clientId": "minio",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : minio_client_secret,
        "redirectUris" : [ f"http://{minio_redirect_uri}/*" ]
    }
    config['clients'].append(new_client)

    config['roles']={
        "client": {
            "minio": [{
            "name": "write"
            }]
        }
    }
else:
    print("Warning! Skipping creating keycloak client for minio")

if ( os.environ.get('LOVD_CLIENT_SECRET') is not None) and ( os.environ.get('LOVD_REDIRECT_URI') is not None):
    lovd_client_secret = os.environ['LOVD_CLIENT_SECRET']
    lovd_redirect_uri = os.environ['LOVD_REDIRECT_URI']
    new_client = {
        "clientId": "lovd",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : lovd_client_secret,
        "redirectUris" : [ f"http://{lovd_redirect_uri}/*" ]
    }
    config['clients'].append(new_client)
else:
    print("Warning! Skipping creating keycloak client for lovd")

if ( os.environ.get('SERVICE_CLIENT_SECRET') is not None) and ( os.environ.get('SERVICE_REDIRECT_URI') is not None):
    service_client_secret = os.environ['SERVICE_CLIENT_SECRET']
    service_redirect_uri = os.environ['SERVICE_REDIRECT_URI']
    new_client = {
        "clientId": "service",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : service_client_secret,
        "redirectUris" : [ f"http://{service_redirect_uri}/*" ]
    }
    config['clients'].append(new_client)
else:
    print("Warning! Skipping creating keycloak client for pmu web ui")
if  os.environ.get('SERVICE_REST_CLIENT_SECRET') is not None:
    service_client_secret = os.environ['SERVICE_REST_CLIENT_SECRET']
    new_client = {
        "clientId": "service-rest",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : service_client_secret,
        "redirectUris" : [ f"" ]
    }
    config['clients'].append(new_client)
else:
    print("Warning! Skipping creating keycloak client for rest api")
if ( os.environ.get('VAST_CLIENT_SECRET') is not None) and ( os.environ.get('VAST_REDIRECT_URI') is not None):
    vast_client_secret = os.environ['VAST_CLIENT_SECRET']
    vast_redirect_uri = os.environ['VAST_REDIRECT_URI']
    new_client = {
        "clientId": "vast",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : vast_client_secret,
        "redirectUris" : [ f"http://{vast_redirect_uri}/*" ]
    }
    config['clients'].append(new_client)
else:
    print("Warning! Skipping creating keycloak client for vast")

with open(config_dir+'config.json', 'w') as outfile:
    json.dump(config, outfile)