import os
oidc_secret = os.environ.get('OIDC_SECRET')
iodc_client_secrets_path = os.environ.get('IODC_CLIENT_SECRETS_PATH')
keycloak_realm = os.environ.get('KEYCLOAK_REALM')


import flask
from flask_oidc import OpenIDConnect
import requests
import http.client
import re
import json
import base64
import gzip
import keycloak_admin
import urllib.parse



ka = keycloak_admin.KeycloakAdmin()

app = flask.Flask(__name__)

app.config.update({
    'SECRET_KEY': oidc_secret,
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': iodc_client_secrets_path,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': keycloak_realm,
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'OIDC_ID_TOKEN_COOKIE_SECURE': True,
})

oidc = OpenIDConnect(app)

valid_groups = set(json.loads(os.environ.get("GROUPS")))

@app.route('/logout', methods=['GET'])
def logout():
  oidc.logout()
  return flask.redirect(oidc.client_secrets['logout_uri']+'?'+
      urllib.parse.urlencode({'redirect_uri': flask.request.host_url}))

@app.route('/index.php', methods=["POST"])
def proxy_async():
  if flask.g.oidc_id_token is None:
    return flask.Response(status=401)
  else:
    return proxy('/index.php')

@app.route('/', defaults={'path': ''}, methods=["GET", "POST", "PUT", "DELETE"])
@app.route('/<path:path>', methods=["GET", "POST", "PUT", "DELETE"])
def proxy_sync(path):
  return proxy(path)

@oidc.require_login
def proxy(path):

  user_id = oidc.user_getfield('sub')
  group = ka.get_group(user_id, valid_groups)

  if group not in valid_groups:
    return "You don't belong to any group", 404
  url = f'vast-{group}:80'
  conn = http.client.HTTPConnection(url)
  headers = dict(flask.request.headers)
  headers.pop('Host', None)
  path = flask.request.environ['RAW_URI']
  try:
    if flask.request.method == 'GET':
      conn.request(flask.request.method, path, headers=headers)
    else:
      conn.request(flask.request.method, path, flask.request.get_data(), headers)
    resp = conn.getresponse()
  except Exception as e:
    return f'Cannot connect to {url}.Error: {str(e)}', 404

  headers=dict(resp.headers)
  headers.pop('Content-Length', None)
  headers.pop('Transfer-Encoding', None)
  content = resp.read()

  flask_response = flask.Response(response=content, headers=headers, status=resp.status)
  return flask_response


if __name__ == '__main__':
  app.run(host='localhost', port=5002, debug=True)