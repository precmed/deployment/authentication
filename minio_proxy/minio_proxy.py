import os
oidc_secret = os.environ.get('OIDC_SECRET')
proxy_url = os.environ.get('PROXY_URL')
iodc_client_secrets_path = os.environ.get('IODC_CLIENT_SECRETS_PATH')
keycloak_url = os.environ.get("KEYCLOAK_URL")
keycloak_realm = os.environ.get('KEYCLOAK_REALM')


import flask
from flask_oidc import OpenIDConnect
import requests
import http.client
import re
import json
import base64
import time
from bs4 import BeautifulSoup
import gzip
import keycloak_admin
import urllib.parse

ka = keycloak_admin.KeycloakAdmin()

app = flask.Flask(__name__)

app.config.update({
    'SECRET_KEY': oidc_secret,
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': iodc_client_secrets_path,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': keycloak_realm,
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'OIDC_ID_TOKEN_COOKIE_SECURE': True,
})

oidc = OpenIDConnect(app)

valid_groups = set(json.loads(os.environ.get("GROUPS")))

users = {}

headers = {
  'User-Agent': 'Mozilla'
}

def b64_decode(b):
  b += "=" * ((4 - len(b) % 4) % 4)
  return base64.b64decode(b)

def get_exp(token):
  return json.loads(b64_decode(token.split('.')[1]))['exp']

def update_token(user):
  r = requests.post('http://'+user['url']+'/minio/webrpc', json={
    'id': 1,  
    'jsonrpc': '2.0',
    'method': 'Web.Login',
    'params': {
      'username': user['access_key'], 
      'password': user['secret_key']
    }
  }, headers = headers)
  token=r.json()['result']['token']
  user['token'] = token

def expires_soon(user):
  return not user['token'] or get_exp(user['token']) - time.time() < 600

@app.route('/logout', methods=['GET'])
def logout():
  oidc.logout()
  return flask.redirect(oidc.client_secrets['logout_uri']+'?'+
      urllib.parse.urlencode({'redirect_uri': flask.request.host_url}))

@app.route('/', defaults={'path': ''}, methods=["GET", "POST", "PUT", "DELETE"])
@app.route('/<path:path>', methods=["GET", "POST", "PUT", "DELETE"])
@oidc.require_login
def proxy(path):

  user_id = oidc.user_getfield('sub')
  group = ka.get_group(user_id, valid_groups)

  if not group:
    return "You don't belong to any group", 404
  if not ka.has_client_role(user_id, "write"):
    group += "-read-only"

  if group not in users:
    group_caps = group.upper().replace("-", "_")
    users[group]={'access_key': os.environ.get(f'S3_{group_caps}_ACCESS_KEY'), 
                  'secret_key': os.environ.get(f'S3_{group_caps}_SECRET_KEY'),
                  'token': '',
                  'url': f's3-ui-{group}:9000'}

  user = users[group]

  try:
    if expires_soon(user):
      update_token(user)
  except Exception as e:
    return f'Cannot connect to {user["url"]}. Error: {str(e)}', 404
  headers = dict(flask.request.headers)
  headers.pop('Host', None)
  headers['Authorization'] = f"Bearer {user['token']}"
  if 'Accept-Encoding' in headers:
    headers['Accept-Encoding'] = 'gzip'
  path = flask.request.environ['RAW_URI']
  conn = http.client.HTTPConnection(user['url'])
  if flask.request.method == 'GET':
    conn.request(flask.request.method, path, headers=headers)
  else:
    conn.request(flask.request.method, path, flask.request.get_data(), headers)
  resp = conn.getresponse()

  headers=dict(resp.headers)
  headers.pop('Content-Length', None)
  content = resp.read()

  if 'Content-Type' in headers and headers['Content-Type'].find('text/html') >= 0:
    if 'Content-Encoding' in headers:
      content = gzip.decompress(content)
    soup = BeautifulSoup(content, 'html.parser')
    body = soup.body
    if body:
      body.attrs["onload"]="login()"
      login_script = soup.new_tag("script")
      login_script.string = """
      function login() {
        if (!localStorage.getItem('token')) {
          localStorage.setItem('token', 'token')
          location.reload()
        }
      }
      """
      body.append(login_script)
      content = soup.prettify('utf-8')
      if 'Content-Encoding' in headers:
        content = gzip.compress(content)

  if 'Location' in headers:
    headers['Location'] = headers['Location'].replace(user['url'], proxy_url)

  flask_response = flask.Response(response=content, headers=headers, status=resp.status)
  return flask_response


# if __name__ == '__main__':
#   app.run(host='localhost', port=5002, debug=True)